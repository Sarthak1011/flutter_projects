import 'package:flutter/material.dart';
import 'Register.dart'; // Import the Register class

class LoginScreen extends StatelessWidget {
  final imageSize = Size(128.0, 128.0); // Adjust the image size as needed.
  final buttonTextSize = 30.0; // Adjust the text size as needed.

  void navigateToRegister(BuildContext context) {
    // Navigate to the "Register" class when the "Login" text is tapped.
    Navigator.push(context, MaterialPageRoute(builder: (context) => Register()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          // Background image - Fullscreen
          Positioned.fill(
            child: Image.asset(
              'lib/assets/collage.jpg',
              fit: BoxFit.cover, // Set to BoxFit.cover to make it fullscreen.
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(bottom: 20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  // Other content or widgets that you want to display above the text.
                  Container(
                    padding: EdgeInsets.all(16.0),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly, // Adjust this to control text spacing.
                    children: [
                      GestureDetector(
                        onTap: () {
                          navigateToRegister(context); // Call navigateToRegister when "Login" text is tapped.
                        },
                        child: Column(
                          children: [
                            SizedBox(
                              width: imageSize.width,
                              height: imageSize.height,
                              child: Image.asset('lib/assets/login.png'), // Replace with the actual image path
                            ),
                            Text(
                              'Login',
                              style: TextStyle(fontSize: buttonTextSize), // Adjust the text size as needed.
                            ),
                          ],
                        ),
                      ),
                      Column(
                        children: [
                          SizedBox(
                            width: imageSize.width,
                            height: imageSize.height,
                            child: Image.asset('lib/assets/form2.png'), // Replace with the actual image path
                          ),
                          Text(
                            'Info',
                            style: TextStyle(fontSize: buttonTextSize), // Adjust the text size as needed.
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
