import 'package:app/FormList.dart';
import 'package:flutter/material.dart';

class AboutEducation extends StatefulWidget {
  @override
  _AboutEducationState createState() => _AboutEducationState();
}

class _AboutEducationState extends State<AboutEducation> {
  final TextEditingController englishController = TextEditingController();
  final TextEditingController mathsController = TextEditingController();
  final TextEditingController scienceController = TextEditingController();
  final TextEditingController totalMarksController = TextEditingController();
  final TextEditingController percentageController = TextEditingController();
  final TextEditingController passingYearController = TextEditingController();

  final TextEditingController physicsController = TextEditingController();
  final TextEditingController chemistryController = TextEditingController();
  final TextEditingController mathsHscController = TextEditingController();
  final TextEditingController pcmTotalMarksController = TextEditingController();
  final TextEditingController totalMarksHscController = TextEditingController();
  final TextEditingController percentageHscController = TextEditingController();
  final TextEditingController passingYearHscController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('About Education'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'SSC',
                style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 16.0),
              _buildForm(
                englishController,
                mathsController,
                scienceController,
                totalMarksController,
                percentageController,
                passingYearController,
              ),
              SizedBox(height: 16.0),
              Text(
                'HSC',
                style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 16.0),
              _buildForm(
                physicsController,
                chemistryController,
                mathsHscController,
                pcmTotalMarksController,
                totalMarksHscController,
                percentageHscController,
                //passingYearHscController,
              ),
              SizedBox(height: 16.0),
              ElevatedButton(
                onPressed: () {
                  // Add your logic to process the form data here
                  print("Form Submitted Successfully");

                  // Show a snackbar to indicate successful form submission
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text('Application form submitted successfully'),
                    ),
                  );

                  // Navigate to the FormList class
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => FormList()),
                  );
                },
                child: Text('Submit'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildForm(
    TextEditingController subject1Controller,
    TextEditingController subject2Controller,
    TextEditingController subject3Controller,
    TextEditingController totalMarksController,
    TextEditingController percentageController,
    TextEditingController passingYearController,
  ) {
    return Form(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildTextField('Physics 1 Marks', subject1Controller),
          _buildTextField('Chemistry 2 Marks', subject2Controller),
          _buildTextField('Maths 3 Marks', subject3Controller),
          _buildTextField('Total Marks', totalMarksController),
          _buildTextField('Percentage', percentageController),
          _buildTextField('Year of Passing', passingYearController),
        ],
      ),
    );
  }

  Widget _buildTextField(String label, TextEditingController controller) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 16.0),
      child: TextFormField(
        controller: controller,
        decoration: InputDecoration(
          labelText: label,
          border: OutlineInputBorder(),
        ),
      ),
    );
  }
}
