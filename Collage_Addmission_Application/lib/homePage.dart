import 'package:app/Year.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          // Background image - Fullscreen
          Positioned.fill(
            child: Image.asset(
              'lib/assets/book.png', // Replace with your background image path
              fit: BoxFit.cover,
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(40.0), // Add padding to the image
                  child: Image.asset(
                    'lib/assets/TSSMLOGO_1-removebg.png', // Replace with your top image path
                    height: 200.0, // Adjust the image height as needed
                  ),
                ),
                SizedBox(height: 40.0), // Add spacing between the image and label

                // Multi-line text with larger font size and width
                RichText(
                  textAlign: TextAlign.center,
                  text: const TextSpan(
                    children: <TextSpan>[
                      TextSpan(
                        text: 'WELCOME TO\n',
                        style: TextStyle(
                          color: Color.fromARGB(255, 23, 23, 23),
                          fontWeight: FontWeight.bold,
                          fontSize: 36.0,
                        ),
                      ),
                      TextSpan(
                        text: 'TSSM BSCOER\n',
                        style: TextStyle(
                          color: Color.fromARGB(255, 23, 23, 23),
                          fontWeight: FontWeight.bold,
                          fontSize: 36.0,
                        ),
                      ),
                      TextSpan(
                        text: 'COLLEGE',
                        style: TextStyle(
                          color: Color.fromARGB(255, 21, 20, 20),
                          fontWeight: FontWeight.bold,
                          fontSize: 36.0,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter, // Align the button to the bottom
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: ElevatedButton(
                onPressed: () {
                  // Navigate to the Year class
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Year()),
                  );
                },
                child: Text('Processed for Admission'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
