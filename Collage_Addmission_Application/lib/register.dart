import 'package:app/Password.dart';
import 'package:flutter/material.dart';
import 'package:app/homePage.dart'; // Import the HomePage class from the correct location

class Register extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body: Stack(
        children: <Widget>[
          // Background image - Fullscreen
          Positioned.fill(
            child: Image.asset(
              'lib/assets/libray.png', // Replace with your background image path
              fit: BoxFit.cover,
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Column(
              children: [
                Image.asset(
                  'lib/assets/logo2 1.png', // Replace with your image path
                  height: 150.0, // Adjust the image height as needed
                ),
                SizedBox(height: 20.0), // Add spacing between the image and text fields
                Container(
                  width: 300.0, // Adjust the width as needed
                  child: TextField(
                    decoration: InputDecoration(labelText: 'Student Email Id'),
                  ),
                ),
                SizedBox(height: 20.0), // Add spacing between text fields
                Container(
                  width: 300.0, // Adjust the width as needed
                  child: TextField(
                    decoration: InputDecoration(labelText: 'Username'),
                  ),
                ),
                SizedBox(height: 20.0), // Add spacing between text fields
                Container(
                  width: 300.0, // Adjust the width as needed
                  child: TextField(
                    decoration: InputDecoration(labelText: 'Password'),
                    obscureText: true, // Hide the password
                  ),
                ),
                SizedBox(height: 20.0), // Add spacing between text fields and button
                Container(
                  width: 300.0, // Adjust the width as needed
                  child: ElevatedButton(
                    onPressed: () {
                      // Navigate to the HomePage
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => HomePage()),
                      );
                    },
                    child: Text('Login'),
                  ),
                ),
                SizedBox(height: 20.0), // Add spacing between the button and the "Forgot Password" label

                // GestureDetector for "Forgot Password"
                GestureDetector(
                  onTap: () {
                    // Navigate to the Password class file
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Password()),
                    );
                  },
                  child: Text(
                    'Forgot Password',
                    style: TextStyle(
                      color: const Color.fromARGB(255, 1, 7, 12), // You can change the color as needed
                      decoration: TextDecoration.underline, // Add underline effect
                    ),
                  ),
                ),
              ],
            ),
          ),
          // Add other widgets or content for registration here
        ],
      ),
    );
  }
}
