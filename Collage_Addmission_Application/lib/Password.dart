import 'package:flutter/material.dart';

class Password extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          // Background image - Fullscreen
          Positioned.fill(
            child: Image.asset(
              'lib/assets/libray.png', // Replace with your background image path
              fit: BoxFit.cover,
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Column(
              children: [
                Image.asset(
                  'lib/assets/logo2 1.png', // Replace with your image path
                  height: 200.0, // Adjust the image height as needed
                ),
                SizedBox(height: 20.0), // Add spacing between the image and text fields
                Container(
                  width: 300.0, // Adjust the width as needed
                  child: TextField(
                    decoration: InputDecoration(labelText: 'Student Email Id'),
                  ),
                ),
                SizedBox(height: 20.0), // Add spacing between text fields
                Container(
                  width: 300.0, // Adjust the width as needed
                  child: TextField(
                    decoration: InputDecoration(labelText: 'Enter New Password'),
                    obscureText: true, // Hide the password
                  ),
                ),
                SizedBox(height: 20.0), // Add spacing between text fields
                Container(
                  width: 300.0, // Adjust the width as needed
                  child: TextField(
                    decoration: InputDecoration(labelText: 'Confirm Password'),
                    obscureText: true, // Hide the password
                  ),
                ),
                SizedBox(height: 20.0), // Add spacing between text fields and button
                Container(
                  width: 300.0, // Adjust the width as needed
                  child: ElevatedButton(
                    onPressed: () {
                      // Add your update password logic here

                      // Navigate back to the Register class
                      child: Text('Update Password');
                      Navigator.pop(context);
                      
                    },
                    child: Text('Update Password'),
                  ),
                ),
              ],
            ),
          ),
          // Add other widgets or content for registration here
        ],
      ),
    );
  }
}
