// FormList.dart


import 'package:flutter/material.dart';
import 'ApplicationForm.dart';

class FormList extends StatefulWidget {
  @override
  _FormListState createState() => _FormListState();
}

class _FormListState extends State<FormList> {
  int selectedForm = 0;
  bool isFormSubmitted = false; // Add this line to initialize isFormSubmitted

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: <Widget>[
            Text('Form List'),
            if (isFormSubmitted) Icon(Icons.check, color: Colors.green),
          ],
        ),
        backgroundColor: Colors.pink,
      ),
      body: Column(
        children: <Widget>[
          SizedBox(height: 20.0),
          RadioListTile(
            title: Text('Application Form', style: TextStyle(fontSize: 20.0)),
            value: 1,
            groupValue: selectedForm,
            onChanged: (value) {
              setState(() {
                selectedForm = value!;
              });

              if (value == 1) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ApplicationForm()),
                ).then((result) {
                  // Handle the result returned from ApplicationForm
                  if (result == true) {
                    setState(() {
                      isFormSubmitted = true;
                    });
                  }
                });
              }
            },
          ),
          RadioListTile(
            title: Text('Library Form', style: TextStyle(fontSize: 20.0)),
            value: 2,
            groupValue: selectedForm,
            onChanged: (value) {
              setState(() {
                selectedForm = value!;
              });
            },
          ),
          RadioListTile(
            title: Text('Eligibility Form', style: TextStyle(fontSize: 20.0)),
            value: 3,
            groupValue: selectedForm,
            onChanged: (value) {
              setState(() {
                selectedForm = value!;
              });
            },
          ),
          RadioListTile(
            title: Text('Anti-Ranging Form', style: TextStyle(fontSize: 20.0)),
            value: 4,
            groupValue: selectedForm,
            onChanged: (value) {
              setState(() {
                selectedForm = value!;
              });
            },
          ),
          RadioListTile(
            title: Text('Document Uploading', style: TextStyle(fontSize: 20.0)),
            value: 5,
            groupValue: selectedForm,
            onChanged: (value) {
              setState(() {
                selectedForm = value!;
              });
              
            },
          ),
          SizedBox(height: 16.0),
          ElevatedButton(
            onPressed: () {
              // Add logic to navigate to selected form based on selectedForm value
              if (selectedForm == 1) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ApplicationForm()),
                ).then((result) {
                  // Handle the result returned from ApplicationForm
                  if (result == true) {
                    setState(() {
                      isFormSubmitted = true;
                    });
                  }
                });
              }
              // Add similar conditions for other forms...
            },
            child: Text('Submit'),
          ),

        ],
      ),
    );
  }
}
