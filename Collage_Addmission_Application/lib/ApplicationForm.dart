import 'package:app/AboutEdu.dart';
import 'package:flutter/material.dart';
//import 'AboutEducation.dart'; // Import the AboutEducation class

class ApplicationForm extends StatefulWidget {
  @override
  _ApplicationFormState createState() => _ApplicationFormState();
}

class _ApplicationFormState extends State<ApplicationForm> {
  final _formKey = GlobalKey<FormState>();

  String _name = '';
  String _fathersName = '';
  String _mothersName = '';
  String _dob = '';
  String _bloodGroup = '';
  String _gender = '';
  String _religionAndCast = '';
  String _studentMobileNo = '';
  String _parentMobileNo = '';
  String _emailId = '';
  String _motherTongue = '';
  String _homeUniversity = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Application Form'),
        backgroundColor: Colors.pink,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Scrollbar(
            child: ListView(
              children: [
                _buildTextField('Name of Candidate', (value) => _name = value),
                _buildTextField("Father's Name", (value) => _fathersName = value),
                _buildTextField("Mother's Name", (value) => _mothersName = value),
                _buildTextField('Date of Birth', (value) => _dob = value),
                _buildTextField('Blood Group', (value) => _bloodGroup = value),
                _buildTextField('Gender', (value) => _gender = value),
                _buildTextField('Religion & Cast', (value) => _religionAndCast = value),
                _buildTextField('Student Mobile No', (value) => _studentMobileNo = value),
                _buildTextField('Parent Mobile No', (value) => _parentMobileNo = value),
                _buildTextField('Email ID', (value) => _emailId = value),
                _buildTextField('Mother Tongue', (value) => _motherTongue = value),
                _buildTextField('Home University', (value) => _homeUniversity = value),
                SizedBox(height: 20.0),
                ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      // Form is valid, navigate to the AboutEducation class
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => AboutEducation()),
                      );
                    }
                  },
                  child: Text('Next'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTextField(String label, Function(String) onChanged) {
    return TextFormField(
      decoration: InputDecoration(labelText: label),
      onChanged: onChanged,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Please enter $label';
        }
        return null;
      },
    );
  }
}
