import 'package:app/FormList.dart';
import 'package:flutter/material.dart';
//import 'package:ApplicationForm.dart'; // Replace with the actual path to ApplicationForm

class Year extends StatefulWidget {
  @override
  _YearState createState() => _YearState();
}

class _YearState extends State<Year> {
  int selectedYear = 0; // To store the selected year, 0 means none selected

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: <Widget>[
            Text('Admission Year'),
            SizedBox(width: 8.0), // Add spacing between the title and image
            Image.asset(
              'lib/assets/admissionIcon 1.png', // Replace with your image path
              height: 40.0, // Adjust the image height as needed
            ),
          ],
        ),
        backgroundColor: Colors.pink, // Set the app bar color to pink
      ),
      body: Column(
        // Move to the top
        children: <Widget>[
          Text(
            'Select the Admission Year', // This label is now at the bottom
            style: TextStyle(
              fontSize: 24.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 20.0), // Add spacing between the label and radio buttons
          RadioListTile(
            title: Text(
              'First Year',
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold), // Increase the font size and apply bold
            ),
            value: 1,
            groupValue: selectedYear,
            onChanged: (value) {
              setState(() {
                selectedYear = value!;
                if (selectedYear == 1) {
                  // Navigate to ApplicationForm
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => FormList()),
                  );
                }
              });
            },
          ),
          RadioListTile(
            title: Text(
              'Second Year',
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold), // Increase the font size and apply bold
            ),
            value: 2,
            groupValue: selectedYear,
            onChanged: (value) {
              setState(() {
                selectedYear = value!;
              });
            },
          ),
          RadioListTile(
            title: Text(
              'Third Year',
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold), // Increase the font size and apply bold
            ),
            value: 3,
            groupValue: selectedYear,
            onChanged: (value) {
              setState(() {
                selectedYear = value!;
              });
            },
          ),
          RadioListTile(
            title: Text(
              'Fourth Year',
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold), // Increase the font size and apply bold
            ),
            value: 4,
            groupValue: selectedYear,
            onChanged: (value) {
              setState(() {
                selectedYear = value!;
              });
            },
          ),
          Expanded(
            child: Image.asset(
              'lib/assets/collage2.png', // Replace with your image path
              fit: BoxFit.fill, // Stretch the image to fill the screen
            ),
          ),
        ],
      ),
    );
  }
}
